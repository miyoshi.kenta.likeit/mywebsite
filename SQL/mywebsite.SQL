CREATE DATABASE my_web_site DEFAULT CHARACTER SET utf8;

CREATE TABLE user (
id int PRIMARY KEY AUTO_INCREMENT,
login_id VARCHAR(256) UNIQUE NOT NULL,
password VARCHAR(256) UNIQUE NOT NULL,
user_name VARCHAR(256) NOT NULL,
create_date datetime NOT NULL,
update_date datetime NOT NULL,
user_profile VARCHAR(256),
greeting VARCHAR(100),
user_age int,
gender VARCHAR(256),
picture_file_name VARCHAR(256));


CREATE TABLE group_info (
id int PRIMARY KEY AUTO_INCREMENT,
admin_id int NOT NULL,
group_name VARCHAR(256) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL,
group_profile VARCHAR(256),
group_greeting VARCHAR(256));

CREATE TABLE user_group (
id int PRIMARY KEY AUTO_INCREMENT,
user_id int NOT NULL,
group_id int NOT NULL,
affiliation_id int NOT NULL);

CREATE TABLE user_to_user (
id int PRIMARY KEY AUTO_INCREMENT,
request_use_id int NOT NULL,
user_id int NOT NULL,
match_id int NOT NULL,
request_time datetime NOT NULL);

CREATE TABLE group_chat (
id int PRIMARY KEY AUTO_INCREMENT,
group_table_id int NOT NULL,
groupchat text NOT NULL,
user_id int NOT NULL,
send_time datetime NOT NULL);

CREATE TABLE user_chat (
id int PRIMARY KEY AUTO_INCREMENT,
chat text NOT NULL,
user_id int NOT NULL,
send_time datetime NOT NULL);
