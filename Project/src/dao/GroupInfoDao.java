package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.GroupInfoBeans;

public class GroupInfoDao {

	//集団情報登録処理
	public static void insertGroupInfo(GroupInfoBeans gib) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO group_info(admin_id,group_name,create_date,update_date,group_profile,group_greeting,fileName)VALUES(?,?,?,?,?,?,?)");

			st.setInt(1, gib.getAdminId());
			st.setString(2, gib.getGroupName());
			st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.setString(5, gib.getGroupPlofile());
			st.setString(6, gib.getGroupGreeting());
			st.setString(7, gib.getFileName());

			st.executeUpdate();
			System.out.println("inserting Group has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//自分がadmin_idである集団以外を検索し、リストに格納する。
	public static ArrayList<GroupInfoBeans> getGroupInfo(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM group_info WHERE admin_id <> ?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			ArrayList<GroupInfoBeans> GroupList = new ArrayList<GroupInfoBeans>();

			while (rs.next()) {
				GroupInfoBeans group = new GroupInfoBeans();
				group.setId(rs.getInt("id"));
				group.setAdminId(rs.getInt("admin_id"));
				group.setGroupName(rs.getString("group_name"));
				group.setCreateDate(rs.getDate("create_date"));
				group.setUpdateDate(rs.getDate("update_date"));
				group.setGroupPlofile(rs.getString("group_profile"));
				group.setGroupGreeting(rs.getString("group_greeting"));
				group.setFileName(rs.getString("fileName"));

				GroupList.add(group);
			}

			System.out.println("getGroupinfo completed");

			return GroupList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//自分がジョインしているor申請している状態の集団情報を取ってくる
	public static ArrayList<GroupInfoBeans> getJoinGroupInfo(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT gi.* FROM group_info gi INNER JOIN (SELECT * FROM user_group WHERE affiliation_id >=1) temp ON gi.id=temp.group_id WHERE temp.user_id=?;");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			ArrayList<GroupInfoBeans> JoinGroupList = new ArrayList<GroupInfoBeans>();

			while (rs.next()) {
				GroupInfoBeans group = new GroupInfoBeans();
				group.setId(rs.getInt("id"));
				group.setAdminId(rs.getInt("admin_id"));
				group.setGroupName(rs.getString("group_name"));
				group.setCreateDate(rs.getDate("create_date"));
				group.setUpdateDate(rs.getDate("update_date"));
				group.setGroupPlofile(rs.getString("group_profile"));
				group.setGroupGreeting(rs.getString("group_greeting"));
				group.setFileName(rs.getString("fileName"));

				JoinGroupList.add(group);
			}

			System.out.println("getJoinGroupinfo completed");

			return JoinGroupList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//集団検索処理の実行、戻り値でリストを返す。
	public static ArrayList<GroupInfoBeans> getGroupFindList(int id, String name, String profile) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM group_info WHERE id <> ?";
			if (!(name.equals(""))) {
				sql += " and group_name LIKE '%" + name + "%'";
			}
			if (!(profile.equals(""))) {
				sql += " and group_profile LIKE '%" + profile + "%'";
			}

			st = con.prepareStatement(sql);
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<GroupInfoBeans> GroupFindList = new ArrayList<GroupInfoBeans>();

			while (rs.next()) {
				GroupInfoBeans group = new GroupInfoBeans();
				group.setId(rs.getInt("id"));
				group.setAdminId(rs.getInt("admin_id"));
				group.setGroupName(rs.getString("group_name"));
				group.setCreateDate(rs.getDate("create_date"));
				group.setUpdateDate(rs.getDate("update_date"));
				group.setGroupPlofile(rs.getString("group_profile"));
				group.setGroupGreeting(rs.getString("group_greeting"));
				group.setFileName(rs.getString("fileName"));

				GroupFindList.add(group);
			}
			System.out.println("getAllUserinfo completed");
			return GroupFindList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//idを引数に集団情報を取ってくるためのDaoだお
	public static GroupInfoBeans getGroupInfoByGroupId(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM group_info WHERE id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			GroupInfoBeans group = new GroupInfoBeans();
			if (rs.next()) {
				group.setId(rs.getInt("id"));
				group.setAdminId(rs.getInt("admin_id"));
				group.setGroupName(rs.getString("group_name"));
				group.setCreateDate(rs.getDate("create_date"));
				group.setUpdateDate(rs.getDate("update_date"));
				group.setGroupPlofile(rs.getString("group_profile"));
				group.setGroupGreeting(rs.getString("group_greeting"));
				group.setFileName(rs.getString("fileName"));

			}

			System.out.println("searching GroupInfo by ID has been completed");

			return group;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//集団に所属している人の数をカウントする
	public static int getGroupMenber(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT COUNT(*) as cnt FROM user_group WHERE group_id=? and affiliation_id=2");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			int groupmenber = 0;
			while (rs.next()) {
				groupmenber = rs.getInt("cnt");

			}

			System.out.println("searching groupmenber has been completed");
			return groupmenber;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//admin_idを引数に集団情報を取ってくるためのDaoだお
		public static GroupInfoBeans getGroupInfoByAdminId(int id) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM group_info WHERE admin_id = ?");
				st.setInt(1, id);

				ResultSet rs = st.executeQuery();

				GroupInfoBeans group = new GroupInfoBeans();
				if (rs.next()) {
					group.setId(rs.getInt("id"));
					group.setAdminId(rs.getInt("admin_id"));
					group.setGroupName(rs.getString("group_name"));
					group.setCreateDate(rs.getDate("create_date"));
					group.setUpdateDate(rs.getDate("update_date"));
					group.setGroupPlofile(rs.getString("group_profile"));
					group.setGroupGreeting(rs.getString("group_greeting"));
					group.setFileName(rs.getString("fileName"));

				}

				System.out.println("searching GroupInfo by ID has been completed");

				return group;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

		//集団情報変更処理のだお
		public static void GroupInfoChange (String groupName, String profile, String greeting, String fileName, int adminId) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("UPDATE group_info SET group_name=?,update_date=?,group_profile=?,group_greeting=?,fileName=? WHERE admin_id=?");
				st.setString(1, groupName);
				st.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				st.setString(3, profile);
				st.setString(4, greeting);
				st.setString(5, fileName);
				st.setInt(6, adminId);

				int rs = st.executeUpdate();
				System.out.println("ユーザ情報変更 OKだよーん。");
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}

}
		//ログインしているユーザがマッチしている集団を、リストに格納する。引数はログインしているユーザのid

		public static ArrayList<GroupInfoBeans> getGroupMatchingList(int id) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("SELECT gi.* FROM group_info gi INNER JOIN(SELECT * FROM user_group WHERE affiliation_id=2) temp ON gi.id=temp.group_id WHERE temp.user_id=?");
				st.setInt(1, id);
				ResultSet rs = st.executeQuery();

				ArrayList<GroupInfoBeans> GroupMatchingList = new ArrayList<GroupInfoBeans>();

				while (rs.next()) {
					GroupInfoBeans group = new GroupInfoBeans();
					group.setId(rs.getInt("id"));
					group.setAdminId(rs.getInt("admin_id"));
					group.setGroupName(rs.getString("group_name"));
					group.setCreateDate(rs.getDate("create_date"));
					group.setUpdateDate(rs.getDate("update_date"));
					group.setGroupPlofile(rs.getString("group_profile"));
					group.setGroupGreeting(rs.getString("group_greeting"));
					group.setFileName(rs.getString("fileName"));

					GroupMatchingList.add(group);
				}

				System.out.println("getGroupMatchingList completed");

				return GroupMatchingList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}


}
