package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.UserInfoBeans;
import beans.UserToUserBeans;

public class UsertoUserDao {

	//ユーザがマッチング申請する処理。
	public static void UserMatchingRequest(int userId, int requestUserId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO user_to_user(request_user_id,user_id,match_id,request_time) VALUES(?,?,?,?)");
			st.setInt(1, requestUserId);
			st.setInt(2, userId);
			st.setInt(3, 1);
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));

			st.executeUpdate();
			System.out.println("inserting usertouser has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}
	//Index1にユーザリスト出力させる際に使用する、ログインしているユーザが申請しているユーザを取得する。

	public static ArrayList<UserToUserBeans> getMyRequestUserId(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT user_id FROM user_to_user WHERE request_user_id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<UserToUserBeans> RequestUseridList = new ArrayList<UserToUserBeans>();

			while (rs.next()) {
				UserToUserBeans user = new UserToUserBeans();
				user.setRequestUserId(rs.getInt("user_id"));
				RequestUseridList.add(user);

			}
			System.out.println("自分が申請したユーザとってきたどー");
			return RequestUseridList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//Index1にユーザリスト出力させる際に使用する、ログインしているユーザが申請しているユーザを取得する。

	public static ArrayList<UserToUserBeans> getMyRequestedByUserId(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT request_user_id FROM user_to_user WHERE user_id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<UserToUserBeans> RequestedUseridList = new ArrayList<UserToUserBeans>();

			while (rs.next()) {
				UserToUserBeans user = new UserToUserBeans();
				user.setUserId(rs.getInt("request_user_id"));
				RequestedUseridList.add(user);

			}
			System.out.println("自分に申請してきたユーザとってきたどー");
			return RequestedUseridList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ログインしているユーザに対し、マッチング申請をしてきているユーザリストを出力(OK)

	public static ArrayList<UserInfoBeans> getMyRequestedUserList(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT u.* FROM user u INNER JOIN(SELECT * FROM user_to_user WHERE match_id = 1) temp ON u.id=temp.request_user_id WHERE temp.user_id=?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<UserInfoBeans> RequestedUserList = new ArrayList<UserInfoBeans>();

			while (rs.next()) {
				UserInfoBeans user = new UserInfoBeans();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("user_name"));
				user.setCreateDate(rs.getDate("create_date"));
				user.setUpdateDate(rs.getDate("update_date"));
				user.setUserProfile(rs.getString("user_profile"));
				user.setGreeting(rs.getString("greeting"));
				user.setUserAge(rs.getInt("user_age"));
				user.setGender(rs.getString("gender"));
				user.setPictureFileName(rs.getString("picture_file_name"));
				user.setFinalLoginDate(rs.getDate("final_login_date"));
				RequestedUserList.add(user);

			}
			System.out.println("自分に申請してきたユーザたちをリストでとってきたぞー");
			return RequestedUserList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//ログインしているユーザに対し、マッチングをしているユーザリストを出力

	public static ArrayList<UserInfoBeans> getMyMatchingList(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT u.* FROM user u INNER JOIN(SELECT * FROM user_to_user WHERE match_id = 2) temp ON u.id=temp.request_user_id WHERE temp.user_id=?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ArrayList<UserInfoBeans> MatchingList = new ArrayList<UserInfoBeans>();

			while (rs.next()) {
				UserInfoBeans user = new UserInfoBeans();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("user_name"));
				user.setCreateDate(rs.getDate("create_date"));
				user.setUpdateDate(rs.getDate("update_date"));
				user.setUserProfile(rs.getString("user_profile"));
				user.setGreeting(rs.getString("greeting"));
				user.setUserAge(rs.getInt("user_age"));
				user.setGender(rs.getString("gender"));
				user.setPictureFileName(rs.getString("picture_file_name"));
				user.setFinalLoginDate(rs.getDate("final_login_date"));
				MatchingList.add(user);

			}
			System.out.println("自分がマッチングしているユーザたち一つ目のリストでとってきたぞー");

			PreparedStatement st1 = null;
			st1 = con.prepareStatement(
					"SELECT u.* FROM user u INNER JOIN(SELECT * FROM user_to_user WHERE match_id = 2) temp ON u.id=temp.user_id WHERE temp.request_user_id=?");
			st1.setInt(1, id);

			ResultSet rs1 = st1.executeQuery();
			while (rs1.next()) {
				UserInfoBeans user = new UserInfoBeans();
				user.setId(rs1.getInt("id"));
				user.setLoginId(rs1.getString("login_id"));
				user.setPassword(rs1.getString("password"));
				user.setUserName(rs1.getString("user_name"));
				user.setCreateDate(rs1.getDate("create_date"));
				user.setUpdateDate(rs1.getDate("update_date"));
				user.setUserProfile(rs1.getString("user_profile"));
				user.setGreeting(rs1.getString("greeting"));
				user.setUserAge(rs1.getInt("user_age"));
				user.setGender(rs1.getString("gender"));
				user.setPictureFileName(rs1.getString("picture_file_name"));
				user.setFinalLoginDate(rs1.getDate("final_login_date"));
				MatchingList.add(user);

			}
			System.out.println("自分がマッチングしているユーザたちをリストでとってきたぞー");
			return MatchingList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//申請を許可した場合の処理を実行するDaoだお。
	public static void UserMatchingConform(int requestedUserId, int requestUserId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE user_to_user SET match_id = 2 WHERE user_id = ? and request_user_id = ?");
			st.setInt(1, requestedUserId);
			st.setInt(2, requestUserId);

			int rs = st.executeUpdate();
			System.out.println("updating match_id 2 in usertouser has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//申請を却下した場合の処理を実行するDaoだお。
	public static void UserMatchingRejected(int requestedUserId, int requestUserId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE user_to_user SET match_id = 0 WHERE user_id = ? and request_user_id = ?");
			st.setInt(1, requestedUserId);
			st.setInt(2, requestUserId);

			int rs = st.executeUpdate();
			System.out.println("updating match_id 0 in usertouser has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//match_id=2のidを取ってくるためのDao

	public static int getId(int id, int userId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT id FROM user_to_user WHERE request_user_id = ? and user_id = ? and match_id = 2");

			st.setInt(1, id);
			st.setInt(2, userId);

			ResultSet rs = st.executeQuery();
			int matchingId = 0;
			while (rs.next()) {
				matchingId = rs.getInt("id");
			}

			if (matchingId == 0) {
				PreparedStatement st1 = null;
				st1 = con.prepareStatement(
						"SELECT id FROM user_to_user WHERE user_id = ? and request_user_id = ? and match_id = 2");

				st1.setInt(1, id);
				st1.setInt(2, userId);
				ResultSet rs1 = st1.executeQuery();
				while (rs1.next()) {
					matchingId = rs1.getInt("id");
				}
			}
				System.out.println("select id in match_id 2 in usertouser has been completed");
				return matchingId;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}
}
