package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class UsertoGroupDao {


	//ユーザがマッチング申請する処理。
		public static void UserToGroupMatchingRequest(int id, int groupId,String textform) throws SQLException {

			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement(
						"INSERT INTO user_group(user_id,group_id,affiliation_id,textform) VALUES(?,?,?,?)");
				st.setInt(1, id);
				st.setInt(2, groupId);
				st.setInt(3, 1);
				st.setString(4, textform);

				st.executeUpdate();
				System.out.println("inserting userto has been completed");
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}

		}

		//集団作成の際、集団作成者を登録する。

				public static void AdminInsert(int id, int groupId) throws SQLException {

					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();
						st = con.prepareStatement(
								"INSERT INTO user_group(user_id,group_id,affiliation_id,textform) VALUES(?,?,?,?)");
						st.setInt(1, id);
						st.setInt(2, groupId);
						st.setInt(3, 2);
						st.setString(4,"集団作成者");

						st.executeUpdate();
						System.out.println("inserting userto has been completed");
					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}

				}

		//申請許可の登録を行うDao

				public static void GroupMatchingSuccess(int id, int groupId) throws SQLException {

					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();
						st = con.prepareStatement(
								"UPDATE user_group SET affiliation_id=2 WHERE user_id=? and group_id=?");
						st.setInt(1, id);
						st.setInt(2, groupId);

						st.executeUpdate();
						System.out.println("updating has success been completed");
					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}

				}
				//申請却下の登録を行うDao

				public static void GroupMatchingRejected(int id, int groupId) throws SQLException {

					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();
						st = con.prepareStatement(
								"UPDATE user_group SET affiliation_id=0 WHERE user_id=? and group_id=?");
						st.setInt(1, id);
						st.setInt(2, groupId);

						st.executeUpdate();
						System.out.println("updating has rejected been completed");
					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}

				}

}
