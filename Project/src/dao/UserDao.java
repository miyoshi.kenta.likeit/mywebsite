package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.GroupRequestedUserBeans;
import beans.UserInfoBeans;
import mysite.MysiteHelper;

public class UserDao {
	// インスタンスオブジェクトを返却させてコードの簡略化

	/**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成する。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void insertUserInfo(UserInfoBeans uib) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO user(login_id,password,user_name,create_date,update_date,user_profile,greeting,user_age,gender,picture_file_name) \n"
							+
							"VALUES(?,?,?,?,?,?,?,?,?,?)");
			st.setString(1, uib.getLoginId());
			st.setString(2, MysiteHelper.ango(uib.getPassword()));
			st.setString(3, uib.getUserName());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			st.setString(6, uib.getUserProfile());
			st.setString(7, uib.getGreeting());
			st.setInt(8, uib.getUserAge());
			st.setString(9, uib.getGender());
			st.setString(10, uib.getPictureFileName());

			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDを取得する
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0とする
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (MysiteHelper.ango(password).equals(rs.getString("password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**ユーザの最終ログイン時間を登録するDao
	 *
	 */
	public static void insertUserLoginDate(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE user SET final_login_date = ? WHERE id = ?");

			st.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
			st.setInt(2,userId);

			int rs = st.executeUpdate();
			System.out.println("logindate OKだよーん。");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}





	/**
	 * ランダムで引数指定分のUserInfoBeansを取得
	 * @param limit 取得したいuserの数 index1で出力
	 * @return <UserInfoBeans>
	 * @throws SQLException
	 */
	public static ArrayList<UserInfoBeans> getAllUserInfo(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();


			st = con.prepareStatement("SELECT * FROM user WHERE id <> ?");
			st.setInt(1, id);


			ResultSet rs = st.executeQuery();

			ArrayList<UserInfoBeans> UserList = new ArrayList<UserInfoBeans>();

			while (rs.next()) {
				UserInfoBeans user = new UserInfoBeans();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("user_name"));
				user.setCreateDate(rs.getDate("create_date"));
				user.setUpdateDate(rs.getDate("update_date"));
				user.setUserProfile(rs.getString("user_profile"));
				user.setGreeting(rs.getString("greeting"));
				user.setUserAge(rs.getInt("user_age"));
				user.setGender(rs.getString("gender"));
				user.setPictureFileName(rs.getString("picture_file_name"));
				user.setFinalLoginDate(rs.getDate("final_login_date"));
				UserList.add(user);
			}
			System.out.println("getAllUserinfo completed");
			return UserList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
       //一人分のユーザ情報を取ってくるためのDaoだお
	public static UserInfoBeans getUserInfoByUserID(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			UserInfoBeans user = new UserInfoBeans();
			if (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("user_name"));
				user.setCreateDate(rs.getDate("create_date"));
				user.setUpdateDate(rs.getDate("update_date"));
				user.setUserProfile(rs.getString("user_profile"));
				user.setGreeting(rs.getString("greeting"));
				user.setUserAge(rs.getInt("user_age"));
				user.setGender(rs.getString("gender"));
				user.setPictureFileName(rs.getString("picture_file_name"));
				user.setFinalLoginDate(rs.getDate("final_login_date"));
			}

			System.out.println("searching userInfo by ID has been completed");

			return user;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザ情報変更処理をするDaoだお。

	public static void UserInfoChange(int id, String password, String Username, String profile, String greeting, int age, String gender, String fileName) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE user SET password = ?,user_name = ?,user_profile = ?,greeting = ?,user_age = ?,gender = ?,update_date =?,picture_file_name = ? WHERE id=?");
			st.setString(1, MysiteHelper.ango(password));
			st.setString(2, Username);
			st.setString(3, profile);
			st.setString(4, greeting);
			st.setInt(5, age);
			st.setString(6, gender);
			st.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			st.setString(8,fileName);
			st.setInt(9, id);

			int rs = st.executeUpdate();
			System.out.println("ユーザ情報変更 OKだよーん。");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}


}

	//ユーザ情報変更処理をするDaoだお。(パスワード変更しないパターン）

		public static void noPasswordUserInfoChange(int id, String Username, String profile, String greeting, int age, String gender, String fileName) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("UPDATE user SET user_name = ?,user_profile = ?,greeting = ?,user_age = ?,gender = ?,update_date =?,picture_file_name = ? WHERE id=?");
				st.setString(1, Username);
				st.setString(2, profile);
				st.setString(3, greeting);
				st.setInt(4, age);
				st.setString(5, gender);
				st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
				st.setString(7,fileName);
				st.setInt(8, id);

				int rs = st.executeUpdate();
				System.out.println("ユーザ情報変更 OKだよーん。");
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}


	}
       //検索フォームの処理を実行するDaoだお
		public static ArrayList<UserInfoBeans> getAllUserFindList(int id, String name, String profile,String age, String age2, String gender ) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE id <> ?";
				if (!(name.equals(""))) {
				sql += " and user_name LIKE '%" + name + "%'";
				}
				if (!(profile.equals(""))) {
					sql += " and user_profile LIKE '%" + profile + "%'";
				}
				if (!(age.equals("(条件無し)"))) {
					sql += " and user_age >= '"+ age +"'";
				}
				if (!(age2.equals("(条件無し)"))) {
					sql += " and user_age <= '"+ age2 + "'";
				}
				if (!(gender.equals("(条件無し)"))) {
					sql += " and gender LIKE '%" + gender + "%'";
				}
				st = con.prepareStatement(sql);
				st.setInt(1, id);

				ResultSet rs = st.executeQuery();

				ArrayList<UserInfoBeans> UserFindList = new ArrayList<UserInfoBeans>();

				while (rs.next()) {
					UserInfoBeans user = new UserInfoBeans();
					user.setId(rs.getInt("id"));
					user.setLoginId(rs.getString("login_id"));
					user.setPassword(rs.getString("password"));
					user.setUserName(rs.getString("user_name"));
					user.setCreateDate(rs.getDate("create_date"));
					user.setUpdateDate(rs.getDate("update_date"));
					user.setUserProfile(rs.getString("user_profile"));
					user.setGreeting(rs.getString("greeting"));
					user.setUserAge(rs.getInt("user_age"));
					user.setGender(rs.getString("gender"));
					user.setPictureFileName(rs.getString("picture_file_name"));
					user.setFinalLoginDate(rs.getDate("final_login_date"));
					UserFindList.add(user);
				}
				System.out.println("getAllUserinfo completed");
				return UserFindList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}


		//グループマッチングで集団に申請してきている人を表示するDao

		public static ArrayList<GroupRequestedUserBeans> getGroupRequestedUserList(int id) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();


				st = con.prepareStatement("SELECT u.*,ug.textform FROM user u INNER JOIN user_group ug ON u.id=ug.user_id INNER JOIN group_info gi ON ug.group_id= gi.id WHERE gi.admin_id=? and ug.affiliation_id=1");
				st.setInt(1, id);


				ResultSet rs = st.executeQuery();

				ArrayList<GroupRequestedUserBeans> UserList = new ArrayList<GroupRequestedUserBeans>();

				while (rs.next()) {
					GroupRequestedUserBeans user = new GroupRequestedUserBeans();
					user.setId(rs.getInt("id"));
					user.setLoginId(rs.getString("login_id"));
					user.setUserName(rs.getString("user_name"));
					user.setCreateDate(rs.getDate("create_date"));
					user.setUpdateDate(rs.getDate("update_date"));
					user.setProfile(rs.getString("user_profile"));
					user.setGreeting(rs.getString("greeting"));
					user.setUserAge(rs.getInt("user_age"));
					user.setGender(rs.getString("gender"));
					user.setPfn(rs.getString("picture_file_name"));
					user.setLoginDate(rs.getDate("final_login_date"));
					user.setTextform(rs.getString("textform"));
					UserList.add(user);
				}
				System.out.println("getGroupRequestedUserList completed");
				return UserList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
		   //集団マッチングにおいて、申請しているユーザの詳細を出力させるためのDao
		public static GroupRequestedUserBeans getUserInfo(int userId) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT u.*,ug.textform FROM user u INNER JOIN user_group ug ON u.id=ug.user_id WHERE u.id=?");
				st.setInt(1, userId);

				ResultSet rs = st.executeQuery();

				GroupRequestedUserBeans user = new GroupRequestedUserBeans();
				if (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLoginId(rs.getString("login_id"));
					user.setUserName(rs.getString("user_name"));
					user.setCreateDate(rs.getDate("create_date"));
					user.setUpdateDate(rs.getDate("update_date"));
					user.setProfile(rs.getString("user_profile"));
					user.setGreeting(rs.getString("greeting"));
					user.setUserAge(rs.getInt("user_age"));
					user.setGender(rs.getString("gender"));
					user.setPfn(rs.getString("picture_file_name"));
					user.setLoginDate(rs.getDate("final_login_date"));
					user.setTextform(rs.getString("textform"));
				}

				System.out.println("searching userInfo by ID has been completed");

				return user;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

}