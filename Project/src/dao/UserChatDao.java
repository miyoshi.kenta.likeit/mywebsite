package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.UserChatBeans;

public class UserChatDao {



	//チャット内容を取ってくるDao
	public static ArrayList<UserChatBeans> getChatData(int tableId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user_chat WHERE table_id=? ORDER BY send_time ASC");

			st.setInt(1,tableId);

			ResultSet rs = st.executeQuery();

			ArrayList<UserChatBeans> ChatDataList = new ArrayList<UserChatBeans>();

			while (rs.next()) {

				UserChatBeans CDL = new UserChatBeans();
				CDL.setId(rs.getInt("id"));
				CDL.setChat(rs.getString("chat"));
				CDL.setUserId(rs.getInt("user_id"));
				CDL.setSendTime(rs.getDate("send_time"));
				CDL.setTableId(rs.getInt("table_id"));
				ChatDataList.add(CDL);
			}

			System.out.println("searching userInfo by ID has been completed");

			return ChatDataList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//チャット内容をデーターベースに登録するDao

	public static void ChatDataRegistration(UserChatBeans ucb) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO user_chat(chat,user_id,send_time,table_id) VALUES(?,?,?,?)");
			st.setString(1, ucb.getChat());
			st.setInt(2, ucb.getUserId());
			st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			st.setInt(4, ucb.getTableId());

			st.executeUpdate();

			System.out.println("inserting Chatdata has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
