package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.GroupChatBeans;
import beans.GroupChatDataBeans;

public class GroupChatDao {

	//グループチャット内容を取ってくるDao
		public static ArrayList<GroupChatDataBeans> getGroupChatData(int groupId) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;

			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT gc.*,u.user_name FROM group_chat gc INNER JOIN user u ON gc.user_id=u.id WHERE gc.group_table_id=?");

				st.setInt(1,groupId);

				ResultSet rs = st.executeQuery();

				ArrayList<GroupChatDataBeans> ChatDataList = new ArrayList<GroupChatDataBeans>();

				while (rs.next()) {

					GroupChatDataBeans gcd = new GroupChatDataBeans();
					gcd.setId(rs.getInt("id"));
					gcd.setChat(rs.getString("groupchat"));
					gcd.setUserId(rs.getInt("user_id"));
					gcd.setSendTime(rs.getDate("send_time"));
					gcd.setGroupTableId(rs.getInt("group_table_id"));
					gcd.setUserName(rs.getString("user_name"));
					ChatDataList.add(gcd);
				}

				System.out.println("searching GroupChat by groupId has been completed");

				return ChatDataList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

		//送信された内容をデータベースに登録する処理。
		public static void GroupChatRegistration(GroupChatBeans gcb) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con= DBManager.getConnection();
				st =  con.prepareStatement("\n" +
						"INSERT INTO group_chat(group_table_id,groupchat,user_id,send_time) VALUES(?,?,?,?)");
				st.setInt(1, gcb.getGroupTableId());
				st.setString(2, gcb.getGroupChat());
				st.setInt(3, gcb.getUserId());
				st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				st.executeUpdate();
				System.out.println("inserting grouopchatdata has been completed");} catch (SQLException e) {
					System.out.println(e.getMessage());
					throw new SQLException(e);
				} finally {
					if (con != null) {
						con.close();
					}
				}




			}
		}


