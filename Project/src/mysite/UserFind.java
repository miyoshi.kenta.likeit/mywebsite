package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import beans.UserToUserBeans;
import dao.UserDao;
import dao.UsertoUserDao;

/**
 * Servlet implementation class UserFind
 */
@WebServlet("/UserFind")
public class UserFind extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserFind() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int userId = (Integer) session.getAttribute("userId");

			String inputUsername = request.getParameter("inputUsername");
			String inputPlofile = request.getParameter("inputPlofile");
			String inputAge = request.getParameter("inputAge");
			String inputAge2 = request.getParameter("inputAge2");
			String inputGender = request.getParameter("inputGender");

			//メソッドを実行させる。検索結果のリストを取得
			ArrayList<UserInfoBeans> UserFindList = UserDao.getAllUserFindList(userId,inputUsername,inputPlofile,inputAge,inputAge2,inputGender);
            //ログインしているユーザが申請している、申請されている、マッチングしているユーザは表示させない処理。
			//自分が申請しているユーザを取得
			ArrayList<UserToUserBeans> RequestUseridList = UsertoUserDao.getMyRequestUserId(userId);

			//自分が申請されているユーザを取得
			ArrayList<UserToUserBeans> RequestedUseridList = UsertoUserDao.getMyRequestedByUserId(userId);

			//これらを自分を除くユーザ全件から、除外
			//まず、自分が申請しているユーザを除外
			for (int i = UserFindList.size() - 1; i >= 0; i--) {
				for (int j = RequestUseridList.size() - 1; j >= 0; j--) {
					if (UserFindList.get(i).getId() == (RequestUseridList.get(j).getRequestUserId())) {
						UserFindList.remove(i);
					}
				}
			}
			//次に自分が申請されているユーザを除外
			for (int i = UserFindList.size() - 1; i >= 0; i--) {
				for (int k = RequestedUseridList.size() - 1; k >= 0; k--) {
					if (UserFindList.get(i).getId() == (RequestedUseridList.get(k).getUserId())) {
						UserFindList.remove(i);
					}
				}

			}

			request.setAttribute("UserFindList",UserFindList);
			request.getRequestDispatcher(MysiteHelper.USER_FIND_LIST_PAGE).forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
