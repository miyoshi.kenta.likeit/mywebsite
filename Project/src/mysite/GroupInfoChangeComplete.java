package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupInfoChangeComplete
 */
@WebServlet("/GroupInfoChangeComplete")
public class GroupInfoChangeComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupInfoChangeComplete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//自分のidを取ってくる
		int id = (Integer) session.getAttribute("userId");
		try {
			String confirme = request.getParameter("botton");
			switch (confirme) {
			case "back":
				//戻るボタンなら、変更確認画面に遷移
				response.sendRedirect("GroupInfoChenge");
				break;

			case "conform":
				String inputGroupname = request.getParameter("inputGroupname");
				String inputProfile = request.getParameter("inputProfile");
				String inputGreeting = request.getParameter("inputGreeting");
				String inputFilename = request.getParameter("inputFile");

				GroupInfoDao.GroupInfoChange(inputGroupname, inputProfile, inputGreeting, inputFilename, id);

				//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
				GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
				int adminId = admin.getAdminId();
				boolean flg = true;
				if(adminId == 0) {
					flg = false;
				}
				request.setAttribute("flg", flg);

				request.getRequestDispatcher(MysiteHelper.GROUP_INFO_CHANGE_COMPLETE_PAGE).forward(request, response);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
