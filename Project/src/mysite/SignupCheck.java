package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.UserInfoBeans;

/**
 * Servlet implementation class SignupCheck
 */

@WebServlet("/SignupCheck")
@MultipartConfig(location="/Users/miyoken/Documents/MyWebSite/Project/WebContent/img", maxFileSize=3048576)
public class SignupCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignupCheck() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();


        //戻るボタンor確認ボタンで分岐させる。
		/**String confirme = request.getParameter("botton");
		switch (confirme) {
		case "back":

			response.sendRedirect("Signup");
			break;

		case "conform":**/
			try {
				String inputLoginId = request.getParameter("loginid");
				String inputPassword = request.getParameter("password");
				String inputPasswordCheck = request.getParameter("passwordCheck");
				String inputUsename = request.getParameter("username");
				String inputProfile = request.getParameter("profile");
				String inputGreeting = request.getParameter("greeting");
				int inputAge = Integer.parseInt(request.getParameter("age"));
				String inputGender = request.getParameter("gender");
				Part inputFile = request.getPart("filename");
				String inputFilename = this.getFileName(inputFile);
				inputFile.write(inputFilename);

				UserInfoBeans uib = new UserInfoBeans();
				uib.setLoginId(inputLoginId);
				uib.setPassword(inputPassword);
				uib.setUserName(inputUsename);
				uib.setUserProfile(inputProfile);
				uib.setGreeting(inputGreeting);
				uib.setUserAge(inputAge);
				uib.setGender(inputGender);
				uib.setPictureFileName(inputFilename);

				String validationMessage = "";

				// 入力されているパスワードが確認用と等しいか確認！
				if (!inputPassword.equals(inputPasswordCheck)) {
					validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
				}

				// ログインIDの入力規則チェック。文字の種類を 英数字 ハイフン アンダースコアのみ入力可能にする！
				if (!MysiteHelper.isLoginIdValidation(uib.getLoginId())) {
					validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます";
				}
				/** loginIdの重複をチェック(未実装)
				if (UserDAO.isOverlapLoginId(uib.getLoginId(), 0)) {
					validationMessage += "ほかのユーザーが使用中のログインIDです";
				}*/

				// バリデーションエラーメッセージがないなら確認画面へ遷移
				if (validationMessage.length() == 0) {
					request.setAttribute("uib", uib);
					request.getRequestDispatcher(MysiteHelper.SIGNUP_CHECK_PAGE).forward(request, response);
				} else {
					session.setAttribute("uib", uib);
					session.setAttribute("validationMessage", validationMessage);
					response.sendRedirect("Signup");
				}

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}

		}

	//}

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
}
}
