package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupDetail1
 */
@WebServlet("/GroupDetail1")
public class GroupDetail1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupDetail1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			//セッションから、userId(自分のid)をとってくる。
			int userId = (Integer) session.getAttribute("userId");

			//詳細ボタンが押され、選択された集団のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("group_id"));

			//インデックスッページから来たか、検索リストから来たかを判別するフラグ
			String flag = request.getParameter("flag");

			//対象の集団情報を取得する。
			GroupInfoBeans group = GroupInfoDao.getGroupInfoByGroupId(id);

			//集団が何人所属しているのかカウントするメソッド実行
			int groupmenber = GroupInfoDao.getGroupMenber(id);

			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(userId);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);


			//リクエストパラメーターにセット
			request.setAttribute("group",group);
			request.setAttribute("flag", flag);

			request.setAttribute("groupmenber",groupmenber);
			request.getRequestDispatcher(MysiteHelper.GROUP_DETAIL1_PAGE).forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
