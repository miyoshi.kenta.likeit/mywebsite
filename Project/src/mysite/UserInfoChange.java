package mysite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserInfoChange
 */
@WebServlet("/UserInfoChange")
public class UserInfoChange extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserInfoChange() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String validationMessage = (String) MysiteHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("validationMessage", validationMessage);
		//自分のidを取ってくる
		int id = (Integer) session.getAttribute("userId");

		try {
			//個人データを取得
			UserInfoBeans myData = UserDao.getUserInfoByUserID(id);

			request.setAttribute("myData", myData);
			request.getRequestDispatcher(MysiteHelper.USER_INFO_CHANGE_PAGE).forward(request, response);
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
