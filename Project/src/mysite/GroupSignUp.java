package mysite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupSignUp
 */
@WebServlet("/GroupSignUp")
@MultipartConfig(location = "/Users/miyoken/Documents/MyWebSite/Project/WebContent/img", maxFileSize = 3048576)
public class GroupSignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupSignUp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//自分のidを取ってくる
		int id = (Integer) session.getAttribute("userId");

		//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)

		try {

			GroupInfoBeans admin = GroupInfoDao.getGroupInfoByAdminId(id);

			int adminId = admin.getAdminId();
			boolean flg = true;
			if (adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);

			//ヘッダーから集団作成が押された場合のフォワード処理
			request.getRequestDispatcher(MysiteHelper.GROUP_SIGNUP_PAGE).forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//自分のidを取ってくる
		int id = (Integer) session.getAttribute("userId");

		//集団作成でフォームに入力された値を取得し、確認ページに遷移させる処理。
		try {
			String inputGroupname = request.getParameter("inputGroupname");
			String inputProfile = request.getParameter("inputProfile");
			String inputGreeting = request.getParameter("inputGreeting");
			Part inputFile = request.getPart("Filename");
			String inputFilename = this.getFileName(inputFile);
			inputFile.write(inputFilename);

			GroupInfoBeans gib = new GroupInfoBeans();
			gib.setGroupName(inputGroupname);
			gib.setGroupPlofile(inputProfile);
			gib.setGroupGreeting(inputGreeting);
			gib.setFileName(inputFilename);

			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);

			request.setAttribute("gib", gib);

			request.getRequestDispatcher(MysiteHelper.GROUP_SIGNUP_CHECK_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}
}
