package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;
import dao.UsertoGroupDao;

/**
 * Servlet implementation class GroupUserDetailProcess
 */
@WebServlet("/GroupUserDetailProcess")
public class GroupUserDetailProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupUserDetailProcess() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			//hiddenから詳細ページで表示されている、ユーザのidを取ってくる。
			int userId = Integer.parseInt(request.getParameter("id"));
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");
			//group_idを取得する。
			GroupInfoBeans group = GroupInfoDao.getGroupInfoByAdminId(id);
			int groupId = group.getId();
			//ボタンの制御による条件分岐
			String Botton = request.getParameter("botton");
				//許可ボタンを押した際の処理
				if (Botton.equals("conform")) {
					UsertoGroupDao.GroupMatchingSuccess(userId, groupId);
					response.sendRedirect("GroupRequestList");

				//却下ボタンを押した際の処理
				}else if (Botton.equals("cansel")) {
					UsertoGroupDao.GroupMatchingRejected(userId, groupId);
					response.sendRedirect("GroupRequestList");

				}







		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
