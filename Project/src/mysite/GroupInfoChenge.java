package mysite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupInfoChenge
 */
@WebServlet("/GroupInfoChenge")
public class GroupInfoChenge extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupInfoChenge() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String validationMessage = (String) MysiteHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("validationMessage", validationMessage);


		//自分のidを取ってくる
				int id = (Integer) session.getAttribute("userId");

				try {
					//個人データを取得
					GroupInfoBeans groupData = GroupInfoDao.getGroupInfoByAdminId(id);

					//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
					GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
					int adminId = admin.getAdminId();
					boolean flg = true;
					if(adminId == 0) {
						flg = false;
					}
					request.setAttribute("flg", flg);

					request.setAttribute("groupData", groupData);
					request.getRequestDispatcher(MysiteHelper.GROUP_INFO_CHANGE_PAGE).forward(request, response);
				} catch (SQLException e) {

					e.printStackTrace();
				}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
