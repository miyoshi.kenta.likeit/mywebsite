package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupDetail2Process
 */
@WebServlet("/GroupDetail2Process")
public class GroupDetail2Process extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupDetail2Process() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");

			//詳細を開いているグループのgroupIdを取ってくる
			int groupId = Integer.parseInt(request.getParameter("groupId"));

			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);

			//ボタンの値で条件分岐させる。
			String confirme = request.getParameter("botton");
			switch (confirme) {
			case "back":

				response.sendRedirect("GroupMatchingList");

				break;

			case "chat":


				response.sendRedirect("ChatCtoG?group_id="+groupId);
				break;
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
