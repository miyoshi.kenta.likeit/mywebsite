package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;
import dao.UsertoGroupDao;

/**
 * Servlet implementation class GroupSignUpCheck
 */
@WebServlet("/GroupSignUpCheck")
public class GroupSignUpCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupSignUpCheck() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//登録完了ページから、インデックスページにリダイレクトさせる処理。
		response.sendRedirect("Index2");
	}

	/**
	 *
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");
			//確認ページにセットされている値を取得
			String inputGroupName = request.getParameter("inputGroupname");
			String inputProfile = request.getParameter("inputProfile");
			String inputGreeting = request.getParameter("inputGreeting");
			String inputFileName = request.getParameter("inputFileName");

			GroupInfoBeans gib = new GroupInfoBeans();
			gib.setAdminId(id);
			gib.setGroupName(inputGroupName);
			gib.setGroupPlofile(inputProfile);
			gib.setGroupGreeting(inputGreeting);
			gib.setFileName(inputFileName);

			//ボタンの押された値によって条件分岐する
			String signupBotton = request.getParameter("signup_botton");

			switch (signupBotton) {
			case "back":
				response.sendRedirect("GroupSignUp");
				break;

			case "regist":
				//登録処理を行う
				GroupInfoDao.insertGroupInfo(gib);
				//自分を集団のメンバーとして登録する
				GroupInfoBeans group = GroupInfoDao.getGroupInfoByAdminId(id);
				int groupId = group.getId();
				UsertoGroupDao.AdminInsert(id, groupId);

				//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
				GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
				int adminId = admin.getAdminId();
				boolean flg = true;
				if(adminId == 0) {
					flg = false;
				}
				request.setAttribute("flg", flg);
				request.getRequestDispatcher(MysiteHelper.GROUP_SIGNUP_COMPLETE_PAGE).forward(request, response);
				break;

			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
