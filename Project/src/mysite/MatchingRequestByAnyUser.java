package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import dao.UsertoUserDao;

/**
 * Servlet implementation class MatchingRequestByAnyUser
 */
@WebServlet("/MatchingRequestByAnyUser")
public class MatchingRequestByAnyUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MatchingRequestByAnyUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int userId = (Integer) session.getAttribute("userId");
			//メソッドを実行させる。
			ArrayList<UserInfoBeans> RequestedUserList = UsertoUserDao.getMyRequestedUserList(userId);
			//(三件ずつ出力させるなどは未実装)
			//リクエストスコープにセットする。
			request.setAttribute("RequestedUserList", RequestedUserList);

			request.getRequestDispatcher(MysiteHelper.MATCHING_REQUEST_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
