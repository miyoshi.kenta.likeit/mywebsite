package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupChatDataBeans;
import beans.GroupInfoBeans;
import dao.GroupChatDao;
import dao.GroupInfoDao;

/**
 * Servlet implementation class ChatCtoG
 */
@WebServlet("/ChatCtoG")
public class ChatCtoG extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChatCtoG() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
		//セッションから、userId(自分のid)をとってくる。
		int userId = (Integer) session.getAttribute("userId");
		//リクエストセットから、グループidを取ってくる
		int groupId = Integer.parseInt(request.getParameter("group_id"));
		//集団の詳細情報を取得する。
		GroupInfoBeans group = GroupInfoDao.getGroupInfoByGroupId(groupId);
		//グループチャットの内容をリストに格納して取ってくる
		ArrayList<GroupChatDataBeans> ChatDataList = GroupChatDao.getGroupChatData(groupId);
		//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
		GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(userId);
		int adminId = admin.getAdminId();
		boolean flg = true;
		if(adminId == 0) {
			flg = false;
		}
		request.setAttribute("flg", flg);

		//自分のid
		request.setAttribute("userId",userId);
		//グループ情報
		request.setAttribute("group",group);
		//チャット内容
		request.setAttribute("ChatDataList", ChatDataList);
		//フォワード
		request.getRequestDispatcher(MysiteHelper.CHAT_CTOG_PAGE).forward(request, response);




		}
		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
