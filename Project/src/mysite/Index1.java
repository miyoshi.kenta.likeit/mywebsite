package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import beans.UserToUserBeans;
import dao.UserDao;
import dao.UsertoUserDao;

/**
 * Servlet implementation class Index1
 */
@WebServlet("/Index1")
public class Index1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Index1() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");

			//ユーザ情報自分を除く全件取得
			ArrayList<UserInfoBeans> UserList = UserDao.getAllUserInfo(id);

			//自分が申請しているユーザを取得
			ArrayList<UserToUserBeans> RequestUseridList = UsertoUserDao.getMyRequestUserId(id);

			//自分が申請されているユーザを取得
			ArrayList<UserToUserBeans> RequestedUseridList = UsertoUserDao.getMyRequestedByUserId(id);

			//これらを自分を除くユーザ全件から、除外
			//まず、自分が申請しているユーザを除外
			for (int i = UserList.size() - 1; i >= 0; i--) {
				bonbon:for (int j = RequestUseridList.size() - 1; j >= 0; j--) {
					if (UserList.get(i).getId() == (RequestUseridList.get(j).getRequestUserId())) {
						UserList.remove(i);
						break bonbon;
					}
				}
			}
			//次に自分が申請されているユーザを除外
			for (int i = UserList.size() - 1; i >= 0; i--) {
				bonbon: for (int k = RequestedUseridList.size() - 1; k >= 0; k--) {
					if (UserList.get(i).getId() == (RequestedUseridList.get(k).getUserId())) {
						UserList.remove(i);
						break bonbon;
					}
				}

			}



			//リクエストスコープにセット
			request.setAttribute("UserList", UserList);

			request.getRequestDispatcher(MysiteHelper.TOP_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
