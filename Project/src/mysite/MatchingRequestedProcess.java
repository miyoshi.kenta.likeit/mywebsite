package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UsertoUserDao;

/**
 * Servlet implementation class MathingRequestedProcess
 */
@WebServlet("/MatchingRequestedProcess")
public class MatchingRequestedProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MatchingRequestedProcess() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		//セッションから、userId(自分のid)をとってくる。
		int requestedUserId = (Integer) session.getAttribute("userId");
		//詳細ボタンが押され、選択されたユーザのIDを型変換し利用
		int requestUserId = Integer.parseInt(request.getParameter("user_id"));
		//ボタンの制御による条件分岐
		String requestedBotton = request.getParameter("requested_botton");
		try {
			//許可ボタンを押した際の処理
			if (requestedBotton.equals("conform")) {
				UsertoUserDao.UserMatchingConform(requestedUserId, requestUserId);
				request.getRequestDispatcher(MysiteHelper.MATCHING_SUCCESS_PAGE).forward(request, response);

			//却下ボタンを押した際の処理
			}else if (requestedBotton.equals("back")) {
				UsertoUserDao.UserMatchingRejected(requestedUserId, requestUserId);
				response.sendRedirect("MatchingRequestByAnyUser");

			}

		}

		/**switch (requestedBotton) {
		case "back":
			response.sendRedirect("Index1");
			break;

		case "conform":
			try {
				//インサート実行。
				UsertoUserDao.UserMatchingRequest(requestUserId, requestedUserId);

				request.getRequestDispatcher(MysiteHelper.TOP_PAGE).forward(request, response);
				break;
			}**/

		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
