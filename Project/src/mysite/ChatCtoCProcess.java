package mysite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserChatBeans;
import beans.UserInfoBeans;
import dao.UserChatDao;
import dao.UserDao;
import dao.UsertoUserDao;

/**
 * Servlet implementation class ChatCtoC
 */
@WebServlet("/ChatCtoCProcess")
public class ChatCtoCProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChatCtoCProcess() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		//セッションから、userId(自分のid)をとってくる。
		int userId = (Integer) session.getAttribute("userId");

		String idStr = request.getParameter("user_id");
		int id = Integer.parseInt(idStr);
		try {

			//ユーザー間のマッチを示す、match_id=2を引数に、user_to_userのidを取ってくる
				int matchingId = UsertoUserDao.getId(id,userId);
				//user_chatテーブルから、table_id=matchingIdを引数に情報を取得する。
				ArrayList<UserChatBeans> ChatDataList = UserChatDao.getChatData(matchingId);
				//相手のユーザ情報をUserDaoから取ってくる
				UserInfoBeans user = UserDao.getUserInfoByUserID(id);

				//user_to_userのid = チャットしているテーブルを
				request.setAttribute("matchingId", matchingId);

				//チャットしている相手のユーザ情報を格納
				request.setAttribute("user", user);

				//自分のidを格納
				request.setAttribute("userId", userId);

				//チャットに関する内容
				request.setAttribute("ChatDataList", ChatDataList);

				//フォワード
				request.getRequestDispatcher(MysiteHelper.CHAT_CTOC_PAGE).forward(request, response);

		}

		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//セッションから、userId(自分のid)をとってくる。
		int userId = (Integer) session.getAttribute("userId");

		int id = Integer.parseInt(request.getParameter("user_id"));
		int matchingId = Integer.parseInt(request.getParameter("matchingId"));

		String inputChat = request.getParameter("inputChat");

		UserChatBeans ucb = new UserChatBeans();
		ucb.setChat(inputChat);
		ucb.setUserId(userId);
		ucb.setTableId(matchingId);

		try {
			UserChatDao.ChatDataRegistration(ucb);

			response.sendRedirect("ChatCtoCProcess?user_id="+id);

		} catch (SQLException e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}





	}

}
