package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UsertoUserDao;

/**
 * Servlet implementation class MatchingRequestProcess
 */
@WebServlet("/MatchingRequestProcess")
public class MatchingRequestProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MatchingRequestProcess() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		//セッションから、userId(自分のid)をとってくる。
		int requestUserId = (Integer) session.getAttribute("userId");
		//詳細ボタンが押され、選択されたユーザのIDを型変換し利用
		int userId = Integer.parseInt(request.getParameter("user_id"));
		//ボタンの制御による条件分岐
		String requestBotton = request.getParameter("request_botton");

		switch (requestBotton) {
		case "back":
			response.sendRedirect("Index1");
			break;

		case "request":
			try {
				//インサート実行。
				UsertoUserDao.UserMatchingRequest(userId, requestUserId);

				response.sendRedirect("Index1");
				break;
			}

			catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
