package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;

/**
 * Servlet implementation class UserInfoChangeCheck
 */
@WebServlet("/UserInfoChangeCheck")
public class UserInfoChangeCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoChangeCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
        //userinfochange.jspからとんできた情報を取得
		HttpSession session = request.getSession();
			try {
				String inputLoginId = request.getParameter("inputloginID");
				String inputPassword = request.getParameter("inputPassword");
				String inputPasswordCheck = request.getParameter("inputPasswordCheck");
				String inputUsername = request.getParameter("inputUsername");
				String inputProfile = request.getParameter("inputProfile");
				String inputGreeting = request.getParameter("inputGreeting");
				int inputAge = Integer.parseInt(request.getParameter("inputAge"));
				String inputGender = request.getParameter("inputGender");
				//既存の写真の値を取得
				String inputFileName = request.getParameter("Filename");
				//変更したい場合、値が入る。
				String inputFilename = request.getParameter("inputFile");
				//変更しない場合(inputFileNameを何も値がない、inputFilenameに代入する)
				if (inputFilename.equals("")) {
					inputFilename += inputFileName ;

				}

				UserInfoBeans uib = new UserInfoBeans();
				uib.setLoginId(inputLoginId);
				uib.setPassword(inputPassword);
				uib.setUserName(inputUsername);
				uib.setUserProfile(inputProfile);
				uib.setGreeting(inputGreeting);
				uib.setUserAge(inputAge);
				uib.setGender(inputGender);
				uib.setPictureFileName(inputFilename);

				String validationMessage = "";

				// 入力されているパスワードが確認用と等しいか確認！
				if (!inputPassword.equals(inputPasswordCheck)) {
					validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
				}
                if (inputUsername.equals(""))
                	validationMessage += "ユーザ名は必ず入力してください<br>";
				// バリデーションエラーメッセージがないなら確認画面へ遷移
				if (validationMessage.length() == 0) {
					request.setAttribute("uib", uib);
					request.getRequestDispatcher(MysiteHelper.USER_INFO_CHANGE_CHECK_PAGE).forward(request, response);
				} else {
					session.setAttribute("uib", uib);
					session.setAttribute("validationMessage", validationMessage);
					response.sendRedirect("UserInfoChange");
				}

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}

		}




	private String inputFileName() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
