package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupInfoChangeCheck
 */
@WebServlet("/GroupInfoChangeCheck")
public class GroupInfoChangeCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupInfoChangeCheck() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//セッションから、userId(自分のid)をとってくる。
		int id = (Integer) session.getAttribute("userId");
		try {
			String inputGroupname = request.getParameter("inputGroupname");
			String inputProfile = request.getParameter("inputProfile");
			String inputGreeting = request.getParameter("inputGreeting");
			String inputFilename = request.getParameter("inputFile");

			GroupInfoBeans gib = new GroupInfoBeans();
			gib.setGroupName(inputGroupname);
			gib.setGroupPlofile(inputProfile);
			gib.setGroupGreeting(inputGreeting);
			gib.setFileName(inputFilename);

			String validationMessage = "";

			if (inputGroupname.equals("")) {
				validationMessage += "集団名を入力してください<br>";
			}
			if (validationMessage.length() == 0) {
				//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
				GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
				int adminId = admin.getAdminId();
				boolean flg = true;
				if(adminId == 0) {
					flg = false;
				}
				request.setAttribute("flg", flg);
				request.setAttribute("gib", gib);
				request.getRequestDispatcher(MysiteHelper.GROUP_INFO_CHANGE_CHECK_PAGE).forward(request, response);
			} else {
				session.setAttribute("gib", gib);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("GroupInfoChenge");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
