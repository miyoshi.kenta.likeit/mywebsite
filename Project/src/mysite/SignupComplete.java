package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import dao.UserDao;


/**
 * Servlet implementation class SignupComplete
 */
@WebServlet("/SignupComplete")
public class SignupComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignupComplete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/**新規登録完了後、ログイン画面に遷移させる**/
		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			String inputLoginId = request.getParameter("loginID");
			String inputPassword = request.getParameter("Password");
			String inputUsename = request.getParameter("Username");
			String inputProfile = request.getParameter("Profile");
			String inputGreeting = request.getParameter("Greeting");
			int inputAge = Integer.parseInt(request.getParameter("age"));
			String inputGender = request.getParameter("Gender");
			String inputFilename = request.getParameter("Filename");

			UserInfoBeans uib = new UserInfoBeans();
			uib.setLoginId(inputLoginId);
			uib.setPassword(inputPassword);
			uib.setUserName(inputUsename);
			uib.setUserProfile(inputProfile);
			uib.setGreeting(inputGreeting);
			uib.setUserAge(inputAge);
			uib.setGender(inputGender);
			uib.setPictureFileName(inputFilename);
			// 登録が確定されたかどうか確認するための変数
			String signupBotton = request.getParameter("signup_botton");

			switch (signupBotton) {
			case "back":
				session.setAttribute("uib", uib);
				response.sendRedirect("Signup");
				break;

			case "regist":
				UserDao.insertUserInfo(uib);
				request.setAttribute("uib", uib);
				request.getRequestDispatcher(MysiteHelper.SIGNUP_COMPRLICATION_PAGE).forward(request, response);
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
