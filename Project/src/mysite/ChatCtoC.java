package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ChatCtoC
 */
@WebServlet("/ChatCtoC")
public class ChatCtoC extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChatCtoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//セッションから、userId(自分のid)をとってくる。
				int userId = (Integer) session.getAttribute("userId");
       //チャット相手のid
				int id = Integer.parseInt(request.getParameter("user_id"));

				String chatBotton = request.getParameter("chat_botton");
				try {
					//チャットへボタンを押した際の処理
					if (chatBotton.equals("goChat")) {

						response.sendRedirect("ChatCtoCProcess?user_id="+id);
					} else if (chatBotton.equals("back")) {
						response.sendRedirect("MatchingList");
					}

				}

				catch (Exception e) {
					e.printStackTrace();
					session.setAttribute("errorMessage", e.toString());
					response.sendRedirect("Error");
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
