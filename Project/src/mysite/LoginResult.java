package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class LoginResult
 */
@WebServlet("/LoginResult")
public class LoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			//パラメーターから取得
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");

			//ユーザーIDを取得
			int userId = UserDao.getUserId(loginId, password);

			//ユーザーIDが取得できたなら
			if (userId != 0) {
				//最終ログイン時間更新するメソッド。DAO
				UserDao.insertUserLoginDate(userId);
				//セッションにセット
				session.setAttribute("isLogin", true);
				session.setAttribute("userId", userId);

				/**ログイン前のページを取得(未実装)
				String returnStrUrl = (String) EcHelper.cutSessionAttribute(session, "returnStrUrl");
				**/

				//ログイン前ページにリダイレクト。指定がない場合Index
				//まだ上記は実装せず、純粋にindex1へ飛ばす
				/**response.sendRedirect(returnStrUrl!=null?returnStrUrl:"Index");**/
				response.sendRedirect("Index1");
			} else {
				session.setAttribute("loginId", loginId);
				session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
				response.sendRedirect("Login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
	}


