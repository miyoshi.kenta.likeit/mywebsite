package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;
import dao.UsertoGroupDao;

/**
 * Servlet implementation class GroupDetail1Process
 */
@WebServlet("/GroupDetail1Process")
public class GroupDetail1Process extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupDetail1Process() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");
			//詳細を開いているグループのgroupIdを取ってくる
			int groupId = Integer.parseInt(request.getParameter("groupId"));
			//フォームのテキストの値を取得
			String Passion = request.getParameter("passion");
			//インデックスor検索結果リストのどちらから来たか判別するフラグを取得
			String Flag = request.getParameter("flag");
			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);
			//戻るor確認ボタンの実行結果によって、制御する。
			String confirme = request.getParameter("botton");
			switch (confirme) {
			case "back":
				if (Flag.equals("index")) {
					response.sendRedirect("Index2");
				} else {
					response.sendRedirect("GroupFind");

				}
				break;

			case "request":
                //ユーザがグループに申請する処理を実行する。
				UsertoGroupDao.UserToGroupMatchingRequest(id, groupId, Passion);
				request.getRequestDispatcher(MysiteHelper.GROUP_REQUEST_COMPLETE_PAGE).forward(request, response);
				break;
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
