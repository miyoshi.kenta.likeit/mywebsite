package mysite;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

public class MysiteHelper {
	// TOPページ(個人間画面)
		static final String TOP_PAGE = "/WEB-INF/jsp/index1.jsp";
	// TOPページ2(個人と集団画面)
		static final String TOP2_PAGE = "/WEB-INF/jsp/index2.jsp";
	// ログイン画面
		static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	// 新規登録ページ
		static final String SIGNUP_PAGE = "/WEB-INF/jsp/signup.jsp";
	// 新規登録確認ページ
		static final String SIGNUP_CHECK_PAGE = "/WEB-INF/jsp/signupCheck.jsp";
	// 新規登録完了ページ
		static final String SIGNUP_COMPRLICATION_PAGE = "/WEB-INF/jsp/signupComplication.jsp";
	// ユーザ詳細ページ(index1から検索してきた検索リストから表示されたユーザ詳細＝ユーザ間で申請がされていないとき)
		static final String USER_DETAIL1_PAGE = "/WEB-INF/jsp/userDetail1.jsp";
	// ユーザ詳細ページ(ユーザ間で申請が行われている場合のユーザ詳細＝申請してきた人リストからの遷移先)
		static final String USER_DETAIL2_PAGE = "/WEB-INF/jsp/userDetail2.jsp";
	// ユーザ詳細ページ(マッチ成立後マッチリストのユーザ詳細)
		static final String USER_DETAIL3_PAGE = "/WEB-INF/jsp/userDetail3.jsp";
	// ユーザ検索結果ページ
		static final String USER_FIND_LIST_PAGE = "/WEB-INF/jsp/userfindlist.jsp";
	// 申請してきた人リストページ
		static final String MATCHING_REQUEST_PAGE = "/WEB-INF/jsp/matchingrequest.jsp";
	// マッチング成功画面
		static final String MATCHING_SUCCESS_PAGE = "/WEB-INF/jsp/matchingsuccess.jsp";
	// マッチしている人一覧画面
		static final String MATCHINGLIST_PAGE = "/WEB-INF/jsp/matchinglist.jsp";
	// 個人間チャット画面
		static final String CHAT_CTOC_PAGE ="/WEB-INF/jsp/chatCtoC.jsp";
	// ユーザ情報変更画面
		static final String USER_INFO_CHANGE_PAGE = "/WEB-INF/jsp/userinfochange.jsp";
	// ユーザ	情報変更確認画面
		static final String USER_INFO_CHANGE_CHECK_PAGE = "/WEB-INF/jsp/userinfochangeCheck.jsp";
	// ユーザ情報変更完了画面
		static final String USER_INFO_CHANGE_COMPLETE_PAGE = "/WEB-INF/jsp/userinfochangecomplete.jsp";
	// 集団作成画面
		static final String GROUP_SIGNUP_PAGE = "/WEB-INF/jsp/groupsignup.jsp";
	// 集団作成確認画面
		static final String GROUP_SIGNUP_CHECK_PAGE = "/WEB-INF/jsp/groupsignupCheck.jsp";
	// 集団作成完了画面
		static final String GROUP_SIGNUP_COMPLETE_PAGE = "/WEB-INF/jsp/groupsignupcomplete.jsp";
	// 集団に申請してきた人リスト(集団管理者専用)
		static final String GROUP_MATCHING_REQUEST_PAGE = "/WEB-INF/jsp/groupmatchingrequest.jsp";
    // 集団に申請してきた個別のユーザ詳細画面
		static final String GROUP_USER_DETAIL_PAGE = "/WEB-INF/jsp/groupuserdetail.jsp";
	// 集団情報変更画面(集団管理者専用)
		static final String GROUP_INFO_CHANGE_PAGE = "/WEB-INF/jsp/groupinfochange.jsp";
	// 集団情報変更確認画面
		static final String GROUP_INFO_CHANGE_CHECK_PAGE = "/WEB-INF/jsp/groupinfochangecheck.jsp";
	// 集団情報変更完了画面
		static final String GROUP_INFO_CHANGE_COMPLETE_PAGE = "/WEB-INF/jsp/groupinfochangecomplete.jsp";
	// 集団検索結果リストページ
		static final String GROUP_FIND_LIST_PAGE = "/WEB-INF/jsp/groupfindlist.jsp";
	// 集団詳細ページ(検索結果リストからと、index2から遷移してきた場合)
		static final String GROUP_DETAIL1_PAGE = "/WEB-INF/jsp/groupdetail1.jsp";
	// 申請内容確認ページ
		static final String GROUP_REQUEST_CHECK_PAGE = "/WEB-INF/jsp/grouprequestCheck.jsp";
	// 申請完了ページ
		static final String GROUP_REQUEST_COMPLETE_PAGE = "/WEB-INF/jsp/grouprequestcomplete.jsp";
	// 所属集団リスト一覧ページ
		static final String MYGROUPLIST_PAGE = "/WEB-INF/jsp/mygrouplist.jsp";
    // 集団詳細ページ(所属している集団の詳細を表示する場合)
		static final String GROUP_DETAIL2_PAGE = "/WEB-INF/jsp/groupdetail2.jsp";
	// 集団チャット画面
		static final String CHAT_CTOG_PAGE ="/WEB-INF/jsp/chatCtoG.jsp";

		public static MysiteHelper getInstance() {
			return new MysiteHelper();
		}
		/**
		 * ログインIDで使える文字種類指定
		 *
		 * @param inputLoginId
		 * @return
		 */
		public static boolean isLoginIdValidation(String inputLoginId) {
			// 英数字アンダースコア以外が入力されていたら
			if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
				return true;
			}

			return false;

		}


		//パスワードを暗号化するやつ。
		public static String ango(String password) {

			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes;
			String result = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

				result = DatatypeConverter.printHexBinary(bytes);

			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();

			}
			return result;
		}

		/**
		 * セッションから指定データを取得（削除も一緒に行う）
		 *
		 * @param session
		 * @param str
		 * @return
		 */
		public static Object cutSessionAttribute(HttpSession session, String str) {
			Object test = session.getAttribute(str);
			session.removeAttribute(str);

			return test;
		}


  /**年齢の入力フォームでの18から100までの表記をさせるメソッド

		public static ageNumber**/















}
