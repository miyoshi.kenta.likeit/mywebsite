package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupChatBeans;
import beans.GroupInfoBeans;
import dao.GroupChatDao;
import dao.GroupInfoDao;

/**
 * Servlet implementation class ChatCtoGProcess
 */
@WebServlet("/ChatCtoGProcess")
public class ChatCtoGProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChatCtoGProcess() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		//セッションから、userId(自分のid)をとってくる。
		int userId = (Integer) session.getAttribute("userId");
		//リクエストセットから、グループidを取ってくる
		int groupId = Integer.parseInt(request.getParameter("group_id"));
		//フォームからチャット内容を取ってくる
		String inputChat = request.getParameter("inputChat");
		//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
		GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(userId);
		int adminId = admin.getAdminId();
		boolean flg = true;
		if(adminId == 0) {
			flg = false;
		}
		request.setAttribute("flg", flg);

		//チャット内容(送信された内容)とその他情報をインスタンスに入れ、データベースに登録する。
		GroupChatBeans gcb = new GroupChatBeans();
		gcb.setGroupChat(inputChat);
		gcb.setUserId(userId);
		gcb.setGroupTableId(groupId);

		GroupChatDao.GroupChatRegistration(gcb);
		response.sendRedirect("ChatCtoG?group_id="+groupId);


		}
		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
