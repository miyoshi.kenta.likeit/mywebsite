package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class Index2
 */
@WebServlet("/Index2")
public class Index2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();


		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");

			//ログインしているユーザが管理者（admin_id）ではない集団の全ての情報を取ってくる。
			ArrayList<GroupInfoBeans> GroupList = GroupInfoDao.getGroupInfo(id);

			//ログインしているユーザがジョインしているor申請している集団を取ってくる。（それは表示させないようにするため。）
			ArrayList<GroupInfoBeans> JoinGroupList = GroupInfoDao.getJoinGroupInfo(id);

			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);

			//GroupListから、JoinGroupListを除外する
			for (int i = GroupList.size() -1; i>=0; i--) {
				bonbon:for (int j = JoinGroupList.size() -1; j >=0; j--) {
					if(GroupList.get(i).getId() == (JoinGroupList.get(j).getId())) {
						GroupList.remove(i);
						break bonbon;

					}
				}
			}


			//セッションに検索ワードが入っていたら破棄する
			String inputGroupName = (String)session.getAttribute("inputGroupName");
			if(inputGroupName != null) {
				session.removeAttribute("inputGroupName");
			}
			String inputProfile = (String)session.getAttribute("inputProfile");
			if(inputProfile != null) {
				session.removeAttribute("inputProfile");
			}


			//リクエストスコープにセット
			request.setAttribute("GroupList", GroupList);

			request.getRequestDispatcher(MysiteHelper.TOP2_PAGE).forward(request, response);

		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {




	}

}
