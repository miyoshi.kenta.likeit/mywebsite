package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupMatchingList
 */
@WebServlet("/GroupMatchingList")
public class GroupMatchingList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupMatchingList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションから、userId(自分のid)をとってくる。
			int userId = (Integer) session.getAttribute("userId");

			//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
			GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(userId);
			int adminId = admin.getAdminId();
			boolean flg = true;
			if(adminId == 0) {
				flg = false;
			}
			request.setAttribute("flg", flg);

            //マッチング(集団)リストを取得し、取ってくる。
			ArrayList<GroupInfoBeans> GroupMatchingList = GroupInfoDao.getGroupMatchingList(userId);
			request.setAttribute("GroupMatchingList",GroupMatchingList);
			request.getRequestDispatcher(MysiteHelper.MYGROUPLIST_PAGE).forward(request, response);

		} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
