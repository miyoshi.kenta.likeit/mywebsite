package mysite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GroupInfoBeans;
import dao.GroupInfoDao;

/**
 * Servlet implementation class GroupFind
 */
@WebServlet("/GroupFind")
public class GroupFind extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupFind() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			//セッションから、userId(自分のid)をとってくる。
			int id = (Integer) session.getAttribute("userId");

			//セッションから検索ワードを取得できれば、そのまま。取れなければ、フォームから送信された値を取得
			String sessioninputGroupName = (String) session.getAttribute("inputGroupName");
			String sessioninputProfile = (String) session.getAttribute("inputProfile");

			//検索フォームからの遷移の場合の処理
			if (sessioninputGroupName == null && sessioninputProfile == null) {

				String inputGroupName = request.getParameter("inputGroupname");
				String inputProfile = request.getParameter("inputProfile");

				//検索結果に一致した集団情報を取得する
				ArrayList<GroupInfoBeans> GroupFindList = GroupInfoDao.getGroupFindList(id, inputGroupName,
						inputProfile);

				//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
				GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
				int adminId = admin.getAdminId();
				boolean flg = true;
				if(adminId == 0) {
					flg = false;
				}
				request.setAttribute("flg", flg);

				//[未実装]ログインしているユーザが申請しているか、所属している集団は表示させない処理

				//セッションに検索ワードをセットする。
				session.setAttribute("inputGroupName", inputGroupName);
				session.setAttribute("inputProfile", inputProfile);

				//集団情報は、リクエストセットする。
				request.setAttribute("GroupFindList", GroupFindList);
				request.getRequestDispatcher(MysiteHelper.GROUP_FIND_LIST_PAGE).forward(request, response);
			}

			//集団詳細ページからバックしてくる場合セッションに値がセットされているのでセッションから、処理実行
			if (sessioninputGroupName != null || sessioninputProfile != null) {
                //メソッド実行
				ArrayList<GroupInfoBeans> GroupFindList = GroupInfoDao.getGroupFindList(id, sessioninputGroupName,
						sessioninputProfile);

				//自分のidを引数にgroup_infoテーブルに、admin_idがあるかないか判別する。(ヘッダーの条件分岐に必要)
				GroupInfoBeans admin =  GroupInfoDao.getGroupInfoByAdminId(id);
				int adminId = admin.getAdminId();
				boolean flg = true;
				if(adminId == 0) {
					flg = false;
				}
				request.setAttribute("flg", flg);

				//集団情報を、リクエストセットする。
				request.setAttribute("GroupFindList", GroupFindList);
				request.getRequestDispatcher(MysiteHelper.GROUP_FIND_LIST_PAGE).forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
