package mysite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class UserInfoChangeComplete
 */
@WebServlet("/UserInfoChangeComplete")
public class UserInfoChangeComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserInfoChangeComplete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//自分のidを取ってくる
		int id = (Integer) session.getAttribute("userId");
		//戻るボタンor確認ボタンで分岐させる。
		String confirme = request.getParameter("botton");
		switch (confirme) {
		case "back":
			//戻るボタンなら、変更確認画面に遷移
			response.sendRedirect("UserInfoChange");
			break;

		case "conform":
			//登録ボタンなら、変更処理を行い、変更完了画面に遷移させる。

			try {

				String inputPassword = request.getParameter("inputPassword");
				String inputUsername = request.getParameter("inputUsername");
				String inputProfile = request.getParameter("inputProfile");
				String inputGreeting = request.getParameter("inputGreeting");
				int inputAge = Integer.parseInt(request.getParameter("inputAge"));
				String inputGender = request.getParameter("inputGender");
				String inputFilename = request.getParameter("Filename");

				if (inputPassword.equals("")) {
					UserDao.noPasswordUserInfoChange(id, inputUsername, inputProfile, inputGreeting, inputAge,
							inputGender, inputFilename);
					request.getRequestDispatcher(MysiteHelper.USER_INFO_CHANGE_COMPLETE_PAGE).forward(request, response);
					break;
				}
				UserDao.UserInfoChange(id, inputPassword, inputUsername, inputProfile, inputGreeting, inputAge,
						inputGender, inputFilename);
				request.getRequestDispatcher(MysiteHelper.USER_INFO_CHANGE_COMPLETE_PAGE).forward(request, response);
				break;

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}
	}
}
