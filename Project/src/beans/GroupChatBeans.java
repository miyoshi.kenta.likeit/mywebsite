package beans;

import java.util.Date;

public class GroupChatBeans {
	private int id;
	private int groupTableId;
	private String groupChat;
	private int userId;
	private Date sendTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGroupTableId() {
		return groupTableId;
	}

	public void setGroupTableId(int groupTableId) {
		this.groupTableId = groupTableId;
	}

	public String getGroupChat() {
		return groupChat;
	}

	public void setGroupChat(String groupChat) {
		this.groupChat = groupChat;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

}
