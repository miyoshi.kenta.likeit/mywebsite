package beans;

import java.util.Date;

public class GroupChatDataBeans {
	private int id;
	private int groupTableId;
	private String chat;
	private int userId;
	private Date sendTime;
	private String userName;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGroupTableId() {
		return groupTableId;
	}
	public void setGroupTableId(int groupTableId) {
		this.groupTableId = groupTableId;
	}
	public String getChat() {
		return chat;
	}
	public void setChat(String chat) {
		this.chat = chat;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}


}
