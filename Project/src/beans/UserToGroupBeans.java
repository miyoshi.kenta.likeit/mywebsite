package beans;

public class UserToGroupBeans {
	private int id;
	private int userId;
	private int groupId;
	private int affiliation;
	private String textform;




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(int affiliation) {
		this.affiliation = affiliation;
	}
	public String getTextform() {
		return textform;
	}

	public void setTextform(String textform) {
		this.textform = textform;
	}

}
