package beans;

import java.util.Date;

public class GroupInfoBeans {
	private int id;
	private int adminId;
	private String groupName;
	private Date createDate;
	private Date updateDate;
	private String groupPlofile;
	private String groupGreeting;
	private String fileName;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getGroupPlofile() {
		return groupPlofile;
	}

	public void setGroupPlofile(String groupPlofile) {
		this.groupPlofile = groupPlofile;
	}

	public String getGroupGreeting() {
		return groupGreeting;
	}

	public void setGroupGreeting(String groupGreeting) {
		this.groupGreeting = groupGreeting;
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
