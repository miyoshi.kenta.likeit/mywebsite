<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>ホーム画面(個人)</title>



    <!-- Bootstrap core CSS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>
    <!-- Custom styles for this template -->
    <link href="CSS/index.css" rel="stylesheet">
    <link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
    <header>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" href="Index2">マッチングサイト</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-controls="navbarCollapse"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="Index1">個人へ<span
							class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link" href="GroupMatchingList">集団リスト</a></li>
					<c:if test="${flg == false}">
					<li class="nav-item"><a class="nav-link" href="GroupSignUp">集団作成</a>
					</li></c:if>
					<c:if test="${flg == true}">
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> 管理者専用 </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="GroupRequestList">・申請者一覧</a> <a
								class="dropdown-item" href="GroupInfoChenge">・集団情報変更</a>

						</div></li></c:if>
				</ul>
				<form class="form-inline mt-2 mt-md-0">

					<button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
				</form>
			</div>
		</nav>
	</header>
<div class="jumbotron jumbotron-fluid">

        <div class="container">
            <h1 class="display-4" align=center>登録完了</h1>
            <p class="lead" align=center>新たな人たちとの出会いに乾杯。</p>
        </div>
    </div>
    <form action="GroupSignUpCheck" method="GET">
    <div class="col-sm-12" align="center">
        <button class="btn btn-lg btn-primary" type="submit">一覧画面に戻る</button>
    </div>
    </form>
    </body></html>