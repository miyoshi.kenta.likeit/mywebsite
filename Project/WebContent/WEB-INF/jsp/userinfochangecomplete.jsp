
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>ホーム画面(個人)</title>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="CSS/index.css" rel="stylesheet">
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="Index1">マッチングサイト</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="Index2">集団へ <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MatchingList">マッチリスト</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MatchingRequestByAnyUser">いいかも</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="UserInfoChange">ユーザ情報</a>
                    </li>
                </ul>
                <form class="form-inline mt-2 mt-md-0">

                    <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
                </form>
            </div>
        </nav>
    </header>
<div class="jumbotron jumbotron-fluid">

        <div class="container">
            <h1 class="display-4" align=center>変更完了しました。</h1>
            <p class="lead" align=center></p>
        </div>
    </div>
    <form action="Index1" method="GET">
    <div class="col-sm-12" align="center">
        <button class="btn btn-lg btn-primary" type="submit">一覧画面に戻る</button>
    </div>
    </form>
    </body></html>