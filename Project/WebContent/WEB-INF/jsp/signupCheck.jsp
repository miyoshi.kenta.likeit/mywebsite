<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>新規登録確認</title>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="Login">マッチングサイト</a>



    </nav>
    <form class="form-signin" action="SignupComplete" method="POST">

        <h1 class="h3 mb-3 font-weight-normal">登録確認画面</h1>
        <label for="inputText">ログインID</label>
        <input type="text" name="loginID" class="form-control" value="${uib.loginId}" readonly>

        <label for="inputPassword">パスワード</label>
        <input type="password" name="Password" class="form-control" value="${uib.password}" readonly>


        <label for="inputText">ユーザ名</label>
        <input type="text" name="Username" class="form-control" value="${uib.userName}" readonly>

        <label for="inputTextarea">プロフィール</label>
        <textarea type="textarea" name="Profile" class="form-control" value="${uib.userProfile}" readonly>${uib.userProfile}</textarea>

        <label for="inputText">一言</label>
        <input type="text" name="Greeting" class="form-control" value="${uib.greeting}" readonly>

        <label for="inputText">年齢</label>
        <input type="text" name="age" value="${uib.userAge}" readonly>

        <label for="inputText">性別</label>
        <select type="text" name="Gender" class="form-control" readonly>
            <option>${uib.gender}</option>

        </select>

        <label for="inputFile">最高の写真</label>
        <!--未実装ファイルアップロード-->
        <img src="img/${uib.pictureFileName}">
        <input type="hidden" value="${uib.pictureFileName}" name="Filename">




        <div class="row">
            <div class="col-sm-6" align="center">
                <button class="btn btn-lg btn-primary" type="submit" name="signup_botton" value="back">戻る</button>
            </div>
            <div class="col-sm-6" align="center">
                <button class="btn btn-lg btn-primary" type="submit" name="signup_botton" value="regist">登録</button>
            </div>

        </div>

    </form>
</body></html>
