<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>ホーム画面(個人)</title>



<!-- Bootstrap core CSS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">


<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="CSS/index.css" rel="stylesheet">
<link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" href="Index2">マッチングサイト</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-controls="navbarCollapse"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="Index1">個人へ<span
							class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link" href="GroupMatchingList">集団リスト</a></li>
					<c:if test="${flg == false}">
					<li class="nav-item"><a class="nav-link" href="GroupSignUp">集団作成</a>
					</li></c:if>
					<c:if test="${flg == true}">
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> 管理者専用 </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="GroupRequestList">・申請者一覧</a> <a
								class="dropdown-item" href="GroupInfoChenge">・集団情報変更</a>

						</div></li></c:if>
				</ul>
				<form class="form-inline mt-2 mt-md-0">

					<button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
				</form>
			</div>
		</nav>
	</header>
	<div class="row">
		<h1 class="col-md-12" align="center">集団詳細</h1>
	</div>
	<div class="card mb-3">
		<div class="row no-gutters">
			<div class="col-md-4">
				<img
					src="https://pbs.twimg.com/profile_images/1142704713156026368/qafj2_Vy_400x400.jpg"
					class="card-img" alt="...">
			</div>
			<div class="col-md-8">
				<div class="card-body">
					<table class="plofile col-12">

						<tbody>
							<tr class="row">
								<td class="col-md-2">集団名</td>
								<td class="col-md-10">${group.groupName}</td>
							</tr>
							<tr class="row">
								<td class="col-md-2">一言</td>
								<td class="col-md-10">${group.groupGreeting}</td>
							</tr>
							<tr class="row">
								<td class="col-md-2">プロフィール</td>
								<td class="col-md-10">${group.groupPlofile}</td>
							</tr>
							<tr class="row">
								<td class="col-md-2">人数</td>
								<td class="col-md-10">${groupmenber}</td>
							</tr>


						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<form action="GroupDetail2Process" method="get">

		<input type="hidden" name="groupId" value="${group.id}">
		<div class="row botton">
			<div class="col-sm-6" align="center">
				<button type="submit" class="btn btn-secondary btn-lg" name="botton"
					value="back">一覧に戻る</button>
			</div>
			<div class="col-sm-6" align="center">
				<button type="submit" class="btn btn-primary  btn-lg" name="botton"
					value="chat">チャットへ</button>
			</div>
		</div>
	</form>
	<p class="box "></p>

	<footer class="container">

		<p class="float-right">
			<a href="#">Back to top</a>
		</p>
		<p>
			&copy; 2017-2020 Company, Inc. &middot; <a href="#">Privacy</a>
			&middot; <a href="#">Terms</a>
		</p>
	</footer>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')
	</script>
	<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
