<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>ログイン画面</title>



<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">

<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="index1">マッチングサイト</a>

		<ul class="navbar-nav">
			<li class="nav-item active"><a class="nav-link" href="Signup">新規登録</a>
			</li>
		</ul>


	</nav>
	<form class="form-signin" action="LoginResult" method="POST">
		<c:if test="${loginErrorMessage != null}">
			<p class="red-text center-align">${loginErrorMessage}</p>
			<br>
		</c:if>
		<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
		<label for="inputLoginID">ログインID</label> <input type="text"
			name="loginId" class="form-control" placeholder="LoginID"
			value="${inputLoginId}" required autofocus> <label
			for="inputPassword">パスワード</label> <input type="password"
			name="password" class="form-control" placeholder="Password"
			required>

		<button class="btn btn-lg btn-primary" type="submit" name="action">ログイン</button>


	</form>
</body>
</html>
