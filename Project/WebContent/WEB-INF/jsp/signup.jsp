<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>新規登録</title>



<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">

<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">マッチングサイト</a>



	</nav>
	<form class="form-signin" action="SignupCheck" method="POST" enctype="multipart/form-data">

		<h1 class="h3 mb-3 font-weight-normal">新規会員登録</h1>
		<label for="inputText">ログインID</label> <input type="text"
			name="loginid" class="form-control" placeholder="LoginID" required
			autofocus> <label for="inputPassword">パスワード</label> <input
			type="password" name="password" class="form-control"
			placeholder="Password" required> <label
			for="inputPasswordCheck">パスワード(確認)</label> <input type="password"
			name="passwordCheck" class="form-control" placeholder="PasswordCheck"
			required> <label for="inputText">ユーザ名</label> <input
			type="text" name="username" class="form-control"
			placeholder="Username" required autofocus> <label
			for="inputTextarea">プロフィール</label>
		<textarea type="textarea" name="profile" class="form-control"
			placeholder="Profile" required autofocus></textarea>

		<label for="inputText">一言</label> <input type="text" name="greeting"
			class="form-control" placeholder="Greeting" required autofocus>

		<label for="inputText">年齢</label> <select type="text" name="age"
			class="form-control" placeholder="Age" required autofocus>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
		</select> <label for="inputText">性別</label> <select type="text" name="gender"
			class="form-control" placeholder="gender" required autofocus>
			<option>男</option>
			<option>女</option>
			<option>選択しない</option>
		</select> <label for="inputFile">最高の写真</label> <input type="file"
			class="form-control-file" name="filename">

		<div class="row"><!--
			<div class="col-sm-6" align="center">
				<button class="btn btn-lg btn-primary" type="submit" name="botton"
					value="back">戻る</button>
			</div> -->
			<div class="col-sm-6" align="center">
				<button class="btn btn-lg btn-primary" type="submit" name="botton"
					value="conform">確認</button>
			</div>

		</div>

	</form>
</body>

</html>
