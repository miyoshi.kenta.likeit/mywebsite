<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>ホーム画面(個人)</title>



<!-- Bootstrap core CSS -->

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">


<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="CSS/chatctoc.css" rel="stylesheet">
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" href="Index2">マッチングサイト</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-controls="navbarCollapse"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="Index1">個人へ<span
							class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link" href="GroupMatchingList">集団リスト</a></li>
					<c:if test="${flg == false}">
					<li class="nav-item"><a class="nav-link" href="GroupSignUp">集団作成</a>
					</li></c:if>
					<c:if test="${flg == true}">
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> 管理者専用 </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="GroupRequestList">・申請者一覧</a> <a
								class="dropdown-item" href="GroupInfoChenge">・集団情報変更</a>

						</div></li></c:if>
				</ul>
				<form class="form-inline mt-2 mt-md-0">

					<button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
				</form>
			</div>
		</nav>
	</header>
	<!-- ▼LINE風ここから -->
	<div class="line__container mx-auto">
		<!-- タイトル -->
		<div class="line__title">
			<a href="GroupDetail2?group_id=${group.id}" class="aaa">${group.groupName}</a>
		</div>

		<!-- ▼会話エリア scrollを外すと高さ固定解除 -->
		<div class="line__contents scroll">
			<c:forEach var="chat" items="${ChatDataList}">
				<c:if test="${chat.userId != userId}">

					<!-- 相手の吹き出し -->
					<div class="line__left">

						<div class="line__left-text">
							<div class="name">${chat.userName}</div>
							<div class="text">${chat.chat}</div>
						</div>
					</div>
				</c:if>

				<c:if test="${chat.userId == userId}">
					<!-- 自分の吹き出し -->
					<div class="line__right">
						<div class="text">${chat.chat}</div>

					</div>
				</c:if>
			</c:forEach>

		</div>
		<!--　▲会話エリア ここまで -->
	</div>
	<!--　▲LINE風ここまで -->
	<form class="chatin mx-auto row" action="ChatCtoGProcess" method="get">
        <input type="hidden" name="group_id" value="${group.id}">
		<input type="text" class="form-control col-10" name="inputChat">

		<button type="submit" class="btn btn-primary col-2">送信</button>

	</form>

</body>
</html>