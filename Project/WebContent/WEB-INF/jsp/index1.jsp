<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v4.1.1">
<title>ホーム画面(個人)</title>



<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">


<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="CSS/index.css" rel="stylesheet">
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" href="Index1">マッチングサイト</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-controls="navbarCollapse"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="Index2">集団へ
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="MatchingList">マッチリスト</a>
					</li>
					<li class="nav-item"><a class="nav-link"
						href="MatchingRequestByAnyUser">いいかも</a></li>
					<li class="nav-item"><a class="nav-link" href="UserInfoChange">ユーザ情報</a>
					</li>
				</ul>
				<form class="form-inline mt-2 mt-md-0" action="" method="">

					<button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
				</form>
			</div>
		</nav>
	</header>

	<div class="container next col-sm-6">
		<h1 align="center">ユーザリスト</h1>
	</div>



	<div class="container matching">

		<!-- Three columns of text below the carousel -->
		<div class="row">
			<c:forEach var="user" items="${UserList}">
				<div class="col-lg-4">
					<img src="../img/${user.pictureFileName}" alt="集団の写真">
					<h2>${user.userName}</h2>
					<p>${user.greeting}</p>
					<p>
						<a class="btn btn-secondary" href="UserDetail1?user_id=${user.id}"
							role="button">詳細&raquo;</a>
					</p>

				</div>
			</c:forEach>
		</div>
	</div>
	<form action="UserFind" method="GET">
			<div class="container serching col-sm-6">
				<h1 align="center">検索フォーム</h1>
				¥
				<div class="form-group">
					<label for="inputUsername">ユーザ名(部分検索)</label> <input type="text"
						class="form-control" name="inputUsername" placeholder="(例)あき">
				</div>
				<div class="form-group">
					<label for="inputplofile">プロフィール(部分検索)</label> <input type="text"
						class="form-control" name="inputPlofile" placeholder="(例)サッカー">
				</div>

				<div class="form-row">
					<div class="form-group col-md-4">
						<label for="inputState">年齢(下限)</label> <select name="inputAge"
							class="form-control">
							<option>(条件無し)</option>
							<option>18</option>
							<option>19</option>
							<option>20</option>
							<option>21</option>
							<option>22</option>
							<option>23</option>
							<option>24</option>
							<option>25</option>
							<option>26</option>
							<option>27</option>
							<option>28</option>
							<option>29</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label></label>
						<p align=center>〜</p>
					</div>
					<div class="form-group col-md-4">
						<label for="inputState">年齢(上限)</label> <select name="inputAge2"
							class="form-control" align="right">
							<option>(条件無し)</option>
							<option>18</option>
							<option>19</option>
							<option>20</option>
							<option>21</option>
							<option>22</option>
							<option>23</option>
							<option>24</option>
							<option>25</option>
							<option>26</option>
							<option>27</option>
							<option>28</option>
							<option>29</option>
						</select>
					</div>

				</div>
				<div class="form-row">
					<div class="form-group col-md-3">
						<label for="inputState">性別</label> <select name="inputGender"
							class="form-control">
							<option>(条件無し)</option>
							<option>男</option>
							<option>女</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12" align="center">
						<button type="submit" class="btn btn-primary">検索</button>
					</div>
				</div>
			</div>
		</form>
		<p class="box"></p>

		<!-- FOOTER -->
		<footer class="container">

			<p class="float-right">
				<a href="#">Back to top</a>
			</p>
			<p>
				&copy; 2020 Company, 0105. &middot; <a href="#">Privacy</a> &middot;
				<a href="#">Terms</a>
			</p>
		</footer>

		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
			integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
			crossorigin="anonymous"></script>
		<script>
			window.jQuery
					|| document
							.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')
		</script>
		<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
