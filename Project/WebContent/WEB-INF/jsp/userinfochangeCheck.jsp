<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>ホーム画面(個人)</title>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>
    <!-- Custom styles for this template -->
    <link href="CSS/index.css" rel="stylesheet">
    <link href="CSS/signin.css" rel="stylesheet">
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="Index1">マッチングサイト</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="Index2">集団へ <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MatchingList">マッチリスト</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MatchingRequestByAnyUser">いいかも</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="UserInfoChange">ユーザ情報</a>
                    </li>
                </ul>
                <form class="form-inline mt-2 mt-md-0">

                    <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">ログアウト</button>
                </form>
            </div>
        </nav>
    </header>
    <form class="form-signin" action="UserInfoChangeComplete" method="post">

        <h1 class="h3 mb-3 font-weight-normal">ユーザ情報変更確認</h1>

        <label for="inputText">ログインID</label>
        <input type="text" name="inputloginID" class="form-control" value="${uib.loginId}" readonly>

        <label for="inputPassword">パスワード</label>
        <input type="password" name="inputPassword" class="form-control" value="${uib.password}" readonly>

        <label for="inputText">ユーザ名</label>
        <input type="text" name="inputUsername" class="form-control" value="${uib.userName}" readonly>

        <label for="inputTextarea">プロフィール</label>
        <textarea type="textarea" name="inputProfile" class="form-control" readonly>${uib.userProfile}</textarea>

        <label for="inputText">一言</label>
        <input type="text" name="inputGreeting" class="form-control" value="${uib.greeting}" readonly>

        <label for="inputText">年齢</label>
        <select type="text" name="inputAge" class="form-control" readonly>
            <option>${uib.userAge}</option>

        </select>

        <label for="inputText">性別</label>
        <select type="text" name="inputGender" class="form-control" readonly>
            <option>${uib.gender}</option>

        </select>

        <label for="inputFile">最高の写真</label>
        <!--未実装　ファイルをアップロードする -->
         <img src="img/${uib.pictureFileName}">
        <input type="hidden" name="Filename" value="${uib.pictureFileName}">


        <div class="row">
            <div class="col-sm-6" align="center">
                <button class="btn btn-lg btn-primary" type="submit" name="botton" value="back">戻る</button>
            </div>
            <div class="col-sm-6" align="center">
                <button class="btn btn-lg btn-primary" type="submit" name="botton" value="conform">登録</button>
            </div>

        </div>

    </form>
</body>
